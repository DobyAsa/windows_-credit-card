namespace HomeWork2_3
{
    class Bank
    {
        class CreditCard
        {
            private int consume;
            private int maxconsume;

            public delegate void SendAllConsumption(int count);

            public event SendAllConsumption BeginMonth;
            public int Consume
            {
                get { return consume; }
            }
            public void Buy(int a)
            {
                if (consume + a <= maxconsume)
                {
                    consume += a;
                }
                else
                {
                    System.Console.WriteLine("剩余额度不足！");
                }
            }
            public void Settle(int day)
            {
                // 当到达还款日的时候，也就是每月四号，开始结算账目，把款项广播给各张储蓄卡
                if (day == 4)
                {
                    System.Console.WriteLine("到达还款日，将扣款{0}", consume);
                    BeginMonth(consume);
                }
            }
            public CreditCard(int max)
            {
                this.consume = 0;
                this.maxconsume = max;
            }
        }

        class DebitCard
        {
            private string name;
            private int balence;
            private static bool Settled;
            public int Balence
            {
                get { return balence; }
            }

            public void Save(int a)
            {
                balence += a;
            }
            public void draw(int a)
            {
                balence -= a;
            }
            public string Name { get { return name; } set { name = value; } }
            public DebitCard(string name)
            {
                this.name = name;
                balence = 0;
                Settled = false;
            }

            public void SettleHandler(int count)
            {
                // 如果没有结算，那么尝试扣款。
                if (!Settled)
                {
                    if (balence - count <= 0)
                        System.Console.WriteLine("{0}余额不足", name);
                    else
                    {
                        balence -= count;
                        Settled = true;
                        System.Console.WriteLine("{0}已成功扣款{1}", name, count);
                    }
                }
            }
        }

        public static void Main(string[] args)
        {
            //定义一张最高消费额十万的信用卡
            CreditCard cdcard = new CreditCard(100000);

            //定义三张储蓄卡
            DebitCard dbcard1 = new DebitCard("card1");
            DebitCard dbcard2 = new DebitCard("card2");
            DebitCard dbcard3 = new DebitCard("card3");

            //往储蓄卡中存钱
            dbcard1.Save(10000);
            dbcard2.Save(9000);
            dbcard3.Save(3000);

            //订阅事件
            cdcard.BeginMonth += dbcard1.SettleHandler;
            cdcard.BeginMonth += dbcard2.SettleHandler;
            cdcard.BeginMonth += dbcard3.SettleHandler;

            //消费4000元
            cdcard.Buy(4000);

            //结算，传递参数为当前日期
            cdcard.Settle(4);
        }

    }
}